var elixir = require('laravel-elixir');

elixir(function (mix) {
    mix.browserify('app.js');
    mix.less('app.less');
});