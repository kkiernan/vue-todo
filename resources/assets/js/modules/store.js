module.exports = {

    STORAGE_KEY: 'todo-tasks',

    save: function(tasks) {
        localStorage.setItem(this.STORAGE_KEY, JSON.stringify(tasks));
    },

    load: function() {
        return JSON.parse(localStorage.getItem(this.STORAGE_KEY)) || [];
    }

};
