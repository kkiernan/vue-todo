var Vue = require('vue');

new Vue({
    el: '#demo',

    components: {
        hello: require('./components/hello'),
        tasks: require('./components/tasks'),
    }
});
