module.exports = {

    template: require('../../../views/hello.html'),

    data: function () {
        return {
            message: 'Hello world'
        }
    },

    directives: {
        disabled: require('../directives/disabled')
    }
    
};
