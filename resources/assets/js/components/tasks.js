var store = require('../modules/store.js');

module.exports = {

    template: require('../../../views/tasks.html'),

    data: function () {
        return {
            tasks: store.load(),

            newTask: {
                name: '',
                isComplete: false,
                notes: ''
            },

            taskFilter: 'all',

            selectedTask: '',

            filters: {
                all: function () {
                    return true;
                },
                active: function (task) {
                    return !task.isComplete;
                },
                completed: function (task) {
                    return task.isComplete;
                }
            }
        }
    },

    ready: function () {
        this.$watch('tasks', function(tasks) {
            store.save(tasks);
        }, {
            deep: true
        });
    },

    filters: {
        filterTasks: function (tasks) {
            return tasks.filter(this.filters[this.taskFilter]);
        }
    },

    computed: {
        remaining: function () {
            return this.tasks.filter(this.filters.active).length;
        },

        completed: function () {
            return this.tasks.filter(this.filters.completed).length;
        },

        noTasksMessage: function () {
            if (this.tasks.filter(this.filters[this.taskFilter]).length > 0) {
                return false;
            }

            if (this.taskFilter === 'all') {
                return 'There are no tasks';
            }

            return 'There are no ' + this.taskFilter + ' tasks';
        }
    },

    directives: {
        disabled: require('../directives/disabled')
    },

    methods: {
        createTask: function (e) {
            e.preventDefault();

            if (!this.newTask.name.trim()) {
                return;
            }

            this.tasks.push(this.newTask);

            this.newTask = {
                name: '',
                isComplete: false,
                notes: ''
            };
        },

        editTask: function (index) {
            this.newTask = this.tasks[index];

            this.tasks.splice(index, 1)

            this.$$.newTaskInput.focus();
        },

        clearCompleted: function () {
            if (!confirm('Are you sure?')) {
                return;
            }

            this.tasks = this.tasks.filter(function(task) {
                return !task.isComplete;
            });
        },

        setTaskFilter: function (filter) {
            this.taskFilter = filter;
        },

        showDetails: function (index) {
            this.selectedTask = this.tasks[index];
            console.log(this.selectedTask);
        }
    }

};
